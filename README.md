# README #

# This script is a usecase of chatbot development applying api.ai API, facebook API,google excel API and python flask. #

### What is this repository for? ###
This script takes bakery order from the facebook messenger (unstructure data) and send it to api.ai to form a structure data via json format.
The json data later will be sent to a heroku instance with python flask backend and get parsed to determine what user need.
The python flask backend then promp the user asking personal info (email, phone, paypal...etc.) and record onto a google spreadsheet (order list).


### How do I get set up? ###

Tested on windows 10

python 3.5 configureation in the heroku backend:
Flask==0.12.1
Flask-Session==0.3.1
google-api-python-client==1.6.2
gspread==0.6.2
jsonschema==2.6.0
oauth2client==4.1.1
oauthlib==2.0.2
Werkzeug==0.13
apiclient==1.0.2

* Deployment instructions
If you have a hard time looking at the log of heroku, you can try ngrok (an executable can provide https url using your local folder)

### Reference ###
[ This script is developed from apiai-weather-webhook-sample tutorial ] ( https://github.com/ShoRit/apiai-weather-webhook-sample-1 )

### Who do I talk to? ###

feel free to reach our for any question. 