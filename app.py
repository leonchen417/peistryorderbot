﻿#!/usr/bin/env python

from __future__ import print_function
#from future.standard_library import install_aliases #for python2 to python3
#install_aliases()
from werkzeug.contrib.cache import SimpleCache
from urllib.parse import urlparse, urlencode
from urllib.request import urlopen, Request
from urllib.error import HTTPError
import gspread
import json
import os
import datetime

from flask import Flask
from flask import request
from flask import make_response
from oauth2client.service_account import ServiceAccountCredentials
# sessionId is the ID of 1 conversation (order+ quantity)
# Flask app should start in global layout
app = Flask(__name__)
cache = SimpleCache()


@app.route('/webhook', methods=['POST']) #under www.xyz/webhook page this is a page of 'listening'.
def webhook(): # parsing the webhook transaction
    req = request.get_json(silent=True, force=True)  #get the json file from the api.ai (webhook)
    
    print("Request:")
    print(json.dumps(req, indent=4))

    res = processRequest(req)

    res = json.dumps(res, indent=4)
    print("Response:")
    print(res)
    r = make_response(res)
    r.headers['Content-Type'] = 'application/json'
    return r

#單一物件導向Single object oriented
def processRequest(req):
    action = req.get("result").get("action")
    Param = req.get("result").get("parameters")
    
    if action =="Order_add":
        ItemNumList = req.get("result").get("parameters").get("ItemNumber")
        AllOrdrClasList = [ItemInfo(eachDictItem) for eachDictItem in ItemNumList]
        OrderTotal = Order_add(AllOrdrClasList)
        res  = make_add_WebhookResult(AllOrdrClasList,OrderTotal)
        cache.set('cache-AllOrdrClasList',AllOrdrClasList,timeout=300)
        cache.set('cache-OrderTotal',OrderTotal,timeout=300)
    elif action =="Order_Change":
        OrderTotal = cache.get('cache-OrderTotal')
        AllOrdrClasList = cache.get('cache-AllOrdrClasList')
        res_hold  = make_add_WebhookResult(AllOrdrClasList,OrderTotal)
        res = Order_change(res_hold.get("speech"))
        
    elif action =="Check_Pay":
        FirstName = req.get("result").get("parameters").get("given-name")
        LastName = req.get("result").get("parameters").get("last-name")
        Phone = req.get("result").get("parameters").get("phone-number")
        DeliverDateTime = req.get("result").get("parameters").get("date-time")
        Email = req.get("result").get("parameters").get("email")
        SideNote = req.get("result").get("parameters").get("side-note")

        OrderTotal = cache.get('cache-OrderTotal')
        AllOrdrClasList = cache.get('cache-AllOrdrClasList')
        Run_DB_input = RunDBInput(FirstName,LastName,Phone,DeliverDateTime,Email,SideNote)
        res = make_check_pay_WebhookResult(OrderTotal)
        #run paypal/confirm BOA banking
    return res

class ItemInfo:
    def __init__(self,eachDictItem):#eachItemDict is the dictionary inside the list
        self.ItemName = eachDictItem.get('OrderItem')
        self.ItemUnitPrice = getItemUnitprice(eachDictItem)
        self.OrderNumber = 1 if eachDictItem.get('number')==None else eachDictItem.get('number')
        self.OrderTotalPrice = self.OrderNumber*self.ItemUnitPrice
        if eachDictItem.get('Flavor_Condition')== "Green Tea":
            self.Flav_Con = "green tea white chocolate "
        elif eachDictItem.get('Flavor_Condition')== "Date Marble":
            self.Flav_Con = "date marble "
        elif eachDictItem.get('Flavor_Condition')== "jars of":
            self.Flav_Con = "jar(s) of "
        elif eachDictItem.get('Flavor_Condition')== "packs of":
            self.Flav_Con = "pack(s) of "
        else:
            self.Flav_Con = " "
        

def getItemUnitprice(eachDictItem):
    if eachDictItem.get('OrderItem') =='pound cake':
	    ItemPrice = 7
    elif eachDictItem.get('OrderItem') =='pudding' or eachDictItem.get('OrderItem')=='cookies':		
	    ItemPrice = 3
    if eachDictItem.get('Flavor_Condition') == 'jars of':
	    AddOnPrice = 2
    else: #current meaning Flavor_Condition = regular/ any flavor
	    AddOnPrice = 0
    CondUnitPrice = ItemPrice + AddOnPrice
    return CondUnitPrice



def Order_add(AllOrdrClasList):
    OrderTotal = sum(EachItem.OrderTotalPrice for EachItem in AllOrdrClasList)
    return OrderTotal

def Order_change(previous_speech):
    previous_speech = previous_speech.replace("you ordered","Your order of")
    speech = previous_speech.replace(". Would you like to check/pay for the amount?"," is canceled. Feel free to make a new order.")
    
    return {
        "speech": speech,
        "displayText": speech,
        # "data": data,
        # "contextOut": [],
        "source": "apiai-weather-webhook-sample"
    }


def make_check_pay_WebhookResult(OrderTotal):
    if OrderTotal is None or OrderTotal ==0:
        speech = "We can't confirm your order, your total is 0. Please order again"
    else:
        #speech ='Please tell me with your FULL name, phone #, email and date and time you would like to pick up. Please pay' + str(OrderTotal)+' dollar via paypal to Peistrybos@gmail.com ! Here is paypal website: "https://www.paypal.com"'
        speech = 'Thanks! let me put that into order ticket! Please visit paypal.com to pay for the order. we will email you once the ticket is paid and approved!'
    
    return {
        "speech": speech,
        "displayText": speech,
        # "data": data,
        # "contextOut": [],
        "source": "apiai-weather-webhook-sample"
    }


def RunDBInput(FirstName,LastName,Phone,DeliverDateTime,Email,SideNote):
    scope = ['https://spreadsheets.google.com/feeds'] #寫死的
    creds = ServiceAccountCredentials.from_json_keyfile_name('Client_secrete.json',scope)
    client = gspread.authorize(creds)
    sheet = client.open('PeistryDB').sheet1
    Col1 = sheet.col_values(1)
    firstNone = Col1.index(next(filter(lambda x:x=='',Col1))) 
    PeistryDB = sheet.get_all_records()
    insertIndex = firstNone+1
    row2Add = [str(insertIndex-1),FirstName,LastName,Phone,Email,cache.get('cache-Order_Items'),str(datetime.datetime.now()),SideNote,'PayTime','DeliveryLoc',DeliverDateTime,cache.get('cache-OrderTotal'),'No']
    sheet.insert_row(row2Add,insertIndex)
    return {}

#res  = makeWebhookResult(AllOrdrClasList,OrderTotal)
def make_add_WebhookResult(AllOrdrClasList,OrderTotal):  
    if OrderTotal is None or OrderTotal ==0:
        speech = "We can't confirm your order, your total is 0. Please order again"
    if AllOrdrClasList is None or AllOrdrClasList==[]:
        speech = "It seems like you didn't order any item, please try again"
    OrderNameList = [EachItem.ItemName for EachItem in AllOrdrClasList]
    OrderNumberList = [EachItem.OrderNumber for EachItem in AllOrdrClasList]
    totalspeech1 = list()
    if OrderTotal != None or OrderTotal ==0:
        OrderNameList = [EachItem.ItemName for EachItem in AllOrdrClasList]
        OrderNumberList = [EachItem.OrderNumber for EachItem in AllOrdrClasList]
        OrderFlav_ConList = [EachItem.Flav_Con for EachItem in AllOrdrClasList]
        for num,flav_con,name in zip(OrderNumberList,OrderFlav_ConList,OrderNameList): #order matters zip -> [[1,3,8],['pound cake(s)','pudding(s)','cookie(s)'],['','','']]
            totalspeech1.append('{} {} {}'.format(num,flav_con,name))
        totalspeech = ' and '.join(totalspeech1)
        speech = "you ordered " + totalspeech + "with totally  " + str(OrderTotal) + " dollars. Would you like to check/pay for the amount?"
        cache.set('cache-OrderTotal',OrderTotal,timeout=300)
        cache.set('cache-Order_Items',totalspeech,timeout=300)
    #print(speech)

    return {
        "speech": speech,
        "displayText": speech,
        # "data": data,
        # "contextOut": [],
        "source": "apiai-peistry-ordering"
    }


if __name__ == '__main__':
    port = int(os.getenv('PORT', 5000))

    print("Starting app on port %d" % port)

    app.run(debug=False, port=port, host='0.0.0.0')

